import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.retec.app',
  appName: 'Retec',
  webDir: 'www',
  bundledWebRuntime: false,
  cordova: {
    preferences: {
      SplashScreen: "background",
      SplashScreenBackgroundColor: "#ffffff",
      SplashScreenHideDuration: "3000",
      SplashScreenWidth: "2732",
      SplashScreenHeight: "2732"
    },
  },
  plugins: {
    SplashScreen: {
      launchAutoHide: false,
      androidScaleType: "CENTER_CROP",
      iosContentMode: "scaleAspectFill",
      backgroundColor: "#ffffff",
      splashFullScreen: false,
      splashImmersive: false,
      splashShowOnlyFirstTime: false,
      splashAutoHide: false,
      splashDelay: 0,
      splashMinAutoHideTime: 3000,
      splashShowSpinner: false,
      androidSplashResourceName: "splash",
      androidSplashDrawableScale: "fill",
      androidSpinnerStyle: "large",
      androidSpinnerColor: "#000000"
    }
  }
};


export default config;
