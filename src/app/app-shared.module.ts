import { TabApprovalComplementosComponent } from './components/tab-approval-complementos/tab-approval-complementos.component';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BodyHeaderComponent } from './components/body-header/body-header.component';
import { CustomCardComponent } from './components/custom-card/custom-card.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { KeyWordInputComponent } from './components/key-word-input/key-word-input.component';
import { NotificationBellComponent } from './components/notification-bell/notification-bell.component';
import { PhotoProfileComponent } from './components/photo-profile/photo-profile.component';
import { TabApprovalDataComponent } from './components/tab-approval-data/tab-approval-data.component';
import { TabAnxFornecedoresComponent } from './components/tab-anx-fornecedores/tab-anx-fornecedores.component';
import { TabApprovalInfoComponent } from './components/tab-approval-info/tab-approval-info.component';
import { TabApprovalMapaComponent } from './components/tab-approval-mapa/tab-approval-mapa.component';
import { TabApprovalEmpresaComponent } from './components/tab-approval-empresa/tab-approval-empresa.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule
  ],
  declarations: [
    BodyHeaderComponent,
    CustomCardComponent,
    FooterComponent,
    HeaderComponent,
    KeyWordInputComponent,
    NotificationBellComponent,
    PhotoProfileComponent,
    TabApprovalDataComponent,
    TabAnxFornecedoresComponent,
    TabApprovalInfoComponent,
    TabApprovalComplementosComponent,
    TabApprovalMapaComponent,
    TabApprovalEmpresaComponent,
  ],
  exports: [
    BodyHeaderComponent,
    CustomCardComponent,
    FooterComponent,
    HeaderComponent,
    KeyWordInputComponent,
    NotificationBellComponent,
    PhotoProfileComponent,
    TabApprovalDataComponent,
    TabAnxFornecedoresComponent,
    TabApprovalInfoComponent,
    TabApprovalComplementosComponent,
    TabApprovalMapaComponent,
    TabApprovalEmpresaComponent
  ]
})

export class AppSharedModules { }
