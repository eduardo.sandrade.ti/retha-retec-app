import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'password-recovery',
    loadChildren: () => import('./pages/password-recovery/password-recovery.module').then( m => m.PasswordRecoveryPageModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('./pages/notifications/notifications.module').then( m => m.NotificationsPageModule)
  },
  {
    path: 'requests',
    loadChildren: () => import('./pages/requests/requests.module').then( m => m.RequestsPageModule)
  },
  {
    path: 'requests-history',
    loadChildren: () => import('./pages/requests-history/requests-history.module').then( m => m.RequestsHistoryPageModule)
  },
  {
    path: 'requests-approval',
    loadChildren: () => import('./pages/requests-approval/requests-approval.module').then( m => m.RequestsApprovalPageModule)
  },
  {
    path: 'requests-approval/:req',
    loadChildren: () => import('./pages/requests-approval-data/requests-approval-data.module').then( m => m.RequestsApprovalDataPageModule)
  },
  {
    path: 'manager-profile',
    loadChildren: () => import('./pages/manager-profile/manager-profile.module').then( m => m.ManagerProfilePageModule)
  },  {
    path: 'splash',
    loadChildren: () => import('./pages/splash/splash.module').then( m => m.SplashPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
