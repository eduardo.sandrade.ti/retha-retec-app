import { NotificationsService } from './services/notifications/notifications.service';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { Router } from '@angular/router';
import { StorageService } from './services/storage/storage.service';
import { User } from './interfaces/user';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './services/api/auth/authentication.service';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {

  userName: any;
  userEmail: any;

  public menu = [
  ];

  constructor(
    public auth: AuthenticationService,
    private platform: Platform,
    private storageService: StorageService,
    private notification: NotificationsService,
    private router: Router,
  ) {
    setInterval(() => {
      this.checkPermissions();
    }, 1000);
  }

  async ngOnInit(): Promise<any> {

    if (!this.storageService.get('server')) {
      this.storageService.set('server', '1');
    }
    if (this.storageService.get('biometric_access') == 'true') {
      this.initializeApp();
      // Get user data when user is already logged
      this.setUserData(await this.auth.userProfile);
      // Bind event of login to get user data
      this.auth.obvEventLogin.subscribe((userLogged: User) => this.setUserData(userLogged));

    } else {
      this.initializeApp();
      // Get user data when user is already logged
      this.setUserData(await this.auth.userProfile);
      // Bind event of login to get user data
      this.auth.obvEventLogin.subscribe((userLogged: User) => this.setUserData(userLogged));
    }
  }

  setUserData(user: User) {
    if (user) {
      this.userEmail = user.data_user?.email;
      this.userName = user.data_user?.nome;
    }
  }

  changeInterface(url: string) {
    this.router.navigate([`${url}`])
  }

  async initializeApp() {
    this.platform.ready().then(() => {
      this.router.navigateByUrl('splash')
    })
  }

  checkPermissions() {
    const permissions = JSON.parse(this.storageService.get('permissions'));
    if (permissions && permissions?.requisicao == true) {

      if (!this.menu.some((object) => JSON.stringify(object) === JSON.stringify({ title: 'Requisições', url: '/requests', icon: 'clipboard' }))) {
        console.log('SIM')
        this.menu.push(
          { title: 'Requisições', url: '/requests', icon: 'clipboard' }
        )
      }
    } else {
      console.log('nao')
      const array = this.menu.filter(function (obj) {
        return obj.title !== "Requisições";
      })
      this.menu = array;
    }
  }
}
