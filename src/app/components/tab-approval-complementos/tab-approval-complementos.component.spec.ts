import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabApprovalComplementosComponent } from './tab-approval-complementos.component';

describe('TabApprovalComplementosComponent', () => {
  let component: TabApprovalComplementosComponent;
  let fixture: ComponentFixture<TabApprovalComplementosComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TabApprovalComplementosComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabApprovalComplementosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
