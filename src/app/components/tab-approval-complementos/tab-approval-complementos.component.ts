import { HttpClient, HttpEventType } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Directory, Filesystem } from '@capacitor/filesystem';
import { ApiService } from 'src/app/services/api/api.service';
import { RequestService } from 'src/app/services/api/requests/request.service';
import { Storage } from '@capacitor/storage';

export const FILE_KEY = 'files';
@Component({
  selector: 'app-tab-approval-complementos',
  templateUrl: './tab-approval-complementos.component.html',
  styleUrls: ['./tab-approval-complementos.component.scss'],
})
export class TabApprovalComplementosComponent implements OnInit {

  @Input() complemento = {};
  @Input() anexos = [];
  downloadProgress = 0;
  downloadUrl = '';
  myFiles = [];
  urlServer: string = this.apiService.getServer();

  constructor(
    private requestService: RequestService,
    private http: HttpClient,
    private apiService: ApiService,
    private fileOpener: FileOpener,
  ) { }

  ngOnInit() { }

  viewAnx(caminho: string) {
    this.downloadProgress = 0;
    this.downloadUrl = caminho;
    this.http.get(this.downloadUrl, {
      responseType: 'blob',
      reportProgress: true,
      observe: 'events',
      headers: this.requestService.getHttpOptions()
    }).subscribe(async event => {
      if (event.type === HttpEventType.DownloadProgress) {
        this.downloadProgress = Math.round((100 * event.loaded) / event.total);
      } else if (event.type === HttpEventType.Response) {
        this.downloadProgress = 0;

        const name = this.downloadUrl.substring(this.downloadUrl.lastIndexOf('/') + 1);
        const base64 = await this.convertBlobToBase64(event.body) as string;

        const savedFile = await Filesystem.writeFile({
          path: name,
          data: base64,
          directory: Directory.Documents
        });
        const path = savedFile.uri;
        console.log(path);
        const mimeType = this.getMimeType(name);

        this.fileOpener.open(path, mimeType)
          .then(() => { console.log('Abriu arquivo') })
          .catch(error => console.log('Erro ao abrir arquivo', error));

        this.myFiles.unshift(path);

        Storage.set({
          key: FILE_KEY,
          value: JSON.stringify(this.myFiles)
        })
      }
    })
  }

  getMimeType(name) {
    if (name.indexOf('pdf') >= 0) {
      return 'application/pdf';
    } else if (name.indexOf('png') >= 0) {
      return 'image/png';
    } else if (name.indexOf('mp4') >= 0) {
      return 'video/mp4';
    }
  }

  convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    }
    reader.readAsDataURL(blob);
  })
}
