import { ApiService } from './../../services/api/api.service';
import { StorageService } from './../../services/storage/storage.service';
import { Component, Input, OnInit, ApplicationRef } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { PhotoService } from 'src/app/services/photo/photo.service';
import { environment } from 'src/environments/environment';
import { interval } from 'rxjs';

@Component({
  selector: 'app-photo-profile',
  templateUrl: './photo-profile.component.html',
  styleUrls: ['./photo-profile.component.scss'],
})
export class PhotoProfileComponent implements OnInit {
  @Input() modeEditEnabled = false;
  constructor(
    public photoService: PhotoService,
    private popoverCtrl: PopoverController,
    private storageService: StorageService,
    private appRef: ApplicationRef,
    private apiService: ApiService
  ) {
    // const source = interval(100); // Define o intervalo para 1 segundo
    // const subscribe = source.subscribe(() => {
    //   this.isBlob = false
    //   const path = JSON.parse(this.storageService.get('user'))
    //   this.photo = path ? path?.path_perfil : ""
    // });
    setInterval(() => {
      this.isBlob = false
      this.urlServer = this.apiService.getServer() ? this.apiService.getServer().replace('/api', '') : environment.server_prod.replace('/api', '')
      const path = JSON.parse(this.storageService.get('user'))
      this.photo = path ? path?.path_perfil : ""
    }, 100);
  }

  urlServer: any = this.apiService.getServer() ? this.apiService.getServer().replace('/api', '') : environment.server_prod.replace('/api', '')

  @Input() photo: string = "";
  @Input() isBlob: boolean = false;

  ngOnInit() {

  }

  async updatePhotoProfile() {
    const photo = await this.photoService.updatePhotoProfile();
    this.isBlob = true;
    this.photo = photo.base64Data;
    console.log(this.photo)
    await this.popoverCtrl.dismiss();
    setTimeout(() => {
      this.appRef.tick();
    }, 1000);
  }

  async removePhotoProfile() {
    await this.photoService.removeUserPhoto();
    this.isBlob = false;
    this.photo = null;
    await this.popoverCtrl.dismiss();
    setTimeout(() => {
      location.reload()
    }, 1000);
  }

}
