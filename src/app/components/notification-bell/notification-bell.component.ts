import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification-bell',
  templateUrl: './notification-bell.component.html',
  styleUrls: ['./notification-bell.component.scss'],
})
export class NotificationBellComponent implements OnInit {
  @Input() boolShowIconNotications = true;
  @Input() intNumberNotifications;
  @Input() darkMode = false;
  @Input() strBkgColorIcon: string;

  constructor() { }

  ngOnInit() {}

}
