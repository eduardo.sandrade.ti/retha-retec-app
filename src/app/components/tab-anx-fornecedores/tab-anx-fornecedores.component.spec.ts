import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabAnxFornecedoresComponent } from './tab-anx-fornecedores.component';

describe('TabAnxFornecedoresComponent', () => {
  let component: TabAnxFornecedoresComponent;
  let fixture: ComponentFixture<TabAnxFornecedoresComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TabAnxFornecedoresComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabAnxFornecedoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
