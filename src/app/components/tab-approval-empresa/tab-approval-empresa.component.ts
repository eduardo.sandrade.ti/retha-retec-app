import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab-approval-empresa',
  templateUrl: './tab-approval-empresa.component.html',
  styleUrls: ['./tab-approval-empresa.component.scss'],
})
export class TabApprovalEmpresaComponent implements OnInit {
  @Input() empresa = {};
  
  constructor() { }

  ngOnInit() { }

}
