import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss'],
})
export class ConfirmModalComponent {

  @Input() message: string;

  constructor(private modalCtrl: ModalController) {}

  async confirm() {
    await this.modalCtrl.dismiss(true);
  }

  async cancel() {
    await this.modalCtrl.dismiss(false);
  }

}
