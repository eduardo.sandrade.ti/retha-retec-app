import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() boolShowNotificationIcon = true;
  @Input() strBkgColorIcon = '#fff';
  @Input() modeEditPhotoProfile = false;
  @Input() photo: string ="";
  @Input() photoIsBlob: boolean =false;

  constructor() { }

  ngOnInit() {
  }

}
