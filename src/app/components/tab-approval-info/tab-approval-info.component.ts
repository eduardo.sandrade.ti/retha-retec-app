import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab-approval-info',
  templateUrl: './tab-approval-info.component.html',
  styleUrls: ['./tab-approval-info.component.scss'],
})
export class TabApprovalInfoComponent implements OnInit {

  @Input() request = {};
  
  constructor() { }

  ngOnInit() { }

}
