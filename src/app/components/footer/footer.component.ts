import { StorageService } from './../../services/storage/storage.service';
import { Component, OnInit } from '@angular/core';

import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {

  appVersion = environment.appVersion;
  server: string = '1'
  constructor(
    private storageService: StorageService
  ) { }

  ngOnInit() {
    this.server = this.storageService.get('server')
  }

}
