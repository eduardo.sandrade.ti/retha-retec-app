import { Injectable } from '@angular/core';
import { Geolocation, Coordinates, Geoposition, PositionError } from '@awesome-cordova-plugins/geolocation/ngx';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class GeolocationService {

  constructor(
    private geolocation: Geolocation
  ) { }

  async getCurrentPosition(): Promise<Coordinates | GeolocationCoordinates | any> {
    return this.geolocation.getCurrentPosition();
  }

  watchPosition(): Observable<Geoposition | PositionError> {
    return this.geolocation.watchPosition();
  }
}
