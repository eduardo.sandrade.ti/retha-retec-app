import { AuthenticationService } from '../api/auth/authentication.service';
import { ApiService } from './../api/api.service';
import { UserPhoto } from './../../interfaces/user';
import { Injectable } from '@angular/core';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { Storage } from '@capacitor/storage';
import { StorageService } from './../storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  public strUrlPhotoProfile: string;

  constructor(
    private storage: StorageService,
    private authenticationService: AuthenticationService
  ) {
    this.getUserPhoto().then((userPhoto: UserPhoto) => {
      if (userPhoto) {
        this.strUrlPhotoProfile = userPhoto.base64Data;
      }
    });
  }

  public async updatePhotoProfile() {
    // Take a photo
    const capturedPhoto: Photo = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100
    });
    // Save the picture and add it to photo collection
    const savedImageFile = await this.savePicture(capturedPhoto);
    await this.authenticationService.updatePhoto(this.strUrlPhotoProfile);
    return savedImageFile;
  }

  public async loadSaved() {
    // Retrieve cached photo array data
    const userPhoto: UserPhoto = await JSON.parse(this.storage.get('user_photo'));
    /*     // Read each saved photo's data from the Filesystem
    const readFile = await Filesystem.readFile({
        path: userPhoto.filepath,
        directory: Directory.Data
    });
    // Web platform only: Load the photo as base64 data
    userPhoto.webviewPath = `data:image/jpeg;base64,${readFile.data}`;
    */
    if (userPhoto) {
      this.strUrlPhotoProfile = userPhoto.base64Data;
    }

  }

  public async getUserPhoto(): Promise<UserPhoto> {
    const userPhoto: UserPhoto = await JSON.parse(this.storage.get('user_photo'));
    return userPhoto;
  }

  public async removeUserPhoto(): Promise<boolean> {
    await this.authenticationService.removePhoto();
    this.strUrlPhotoProfile = null;
    return true;
  }


  private async savePicture(photo: Photo): Promise<UserPhoto> {
    // Convert photo to base64 format, required by Filesystem API to save
    const base64Data = await this.readAsBase64(photo);
    // Write the file to the data directory
    const fileName = new Date().getTime() + '.jpeg';
    /* const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: Directory.Data
    }); */

    // Use webPath to display the new image instead of base64 since it's already loaded into memory
    const objPhotoToSave: UserPhoto = {
      filepath: fileName,
      base64Data,
      webviewPath: photo.webPath
    };
    // Set property with base 64 value (temporary)
    this.strUrlPhotoProfile = base64Data;
    return objPhotoToSave;
  }

  private async readAsBase64(photo: Photo) {
    // Fetch the photo, read as a blob, then convert to base64 format
    const response = await fetch(photo.webPath);
    const blob = await response.blob();

    return await this.convertBlobToBase64(blob) as string;
  }

  private convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

}
