import { HttpClient } from '@angular/common/http';
import { environment } from './../../../../environments/environment.sandbox';
import { StorageService } from './../../storage/storage.service';
import { NavController } from '@ionic/angular';
import { ApiService } from './../api.service';
import { Injectable } from '@angular/core';
import { NotificationsService } from '../../notifications/notifications.service';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(
    private api: ApiService,
    private http: HttpClient
  ) { }


  myRequests() {
    return this.api.get('/requisicoes/me')
  }

  getRequest(hashCode: string) {
    return this.api.get(`/requisicoes/${hashCode}`)
  }

  getUrlPdf(hashCode: string) {
    return `${this.api.getServer()}/requisicoes/${hashCode}/mapa/pdf`
  }

  aprovar(hashCode: string, aprovar: any) {
    return this.api.post(`/requisicoes/${hashCode}/aprovar`, aprovar)
  }

  myHistory() {
    return this.api.get('/requisicoes/historico')
  }

  getHttpOptions() {
    return this.api.getHttpOptions();
  }
}
