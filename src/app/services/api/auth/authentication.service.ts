import { Router } from '@angular/router';
import { Observable } from 'rxjs';
/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { Coordinates } from '@awesome-cordova-plugins/geolocation/ngx';
import { FingerprintAIO, FingerprintOptions } from '@ionic-native/fingerprint-aio/ngx';

import { ApiService } from '../api.service';
import { BodyRequestLogin } from '../../../interfaces/user';
import { GeolocationService } from '../../geolocation/geolocation.service';
import { NotificationsService } from '../../notifications/notifications.service';
import { StorageService } from '../../storage/storage.service';
import { User } from 'src/app/interfaces/user';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public boolIsLoggedIn = false;
  readonly messageError = 'Oops, algo deu errado! Tente novamente mais tarde.';
  fingerprintOptions: FingerprintOptions;
  public obvEventLogin = new Subject<User>();
  public userLogged: User;

  constructor(
    private api: ApiService,
    private fingerprint: FingerprintAIO,
    private geolocationService: GeolocationService,
    private menuCtrl: MenuController,
    private notifications: NotificationsService,
    private navCtrl: NavController,
    protected storage: StorageService,
  ) {
    this.fingerprintOptions = {};
    this.fingerprintOptions.cancelButtonTitle = 'Cancelar';
    this.fingerprintOptions.fallbackButtonTitle = 'Titulo do botão de falha';
    this.fingerprintOptions.title = 'Retec';
    this.fingerprintOptions.subtitle = '';
    this.fingerprintOptions.description = 'Desbloqueie seu celular';
    this.fingerprintOptions.disableBackup = false;
  }

  get userProfile() {
    return JSON.parse(this.storage.get('user_profile'));
  }

  async getUserProfile() {
    const user: User = await JSON.parse(this.storage.get('user_profile'));
    return user;
  }

  saveUser(user: User | any): void {
    this.storage.set('token', user.access_token);
    this.storage.set('user', JSON.stringify(user.usuario));
    this.storage.set('contato', JSON.stringify(user.contato));
    this.storage.set('permissions', JSON.stringify(user.permissions));
  }

  removeUser(): Promise<any> {
    return this.storage.remove('user_profile');
  }

  setBiometricAccess(isActive: boolean) {
    return this.storage.set('biometric_access', isActive);
  }

  async loadUser() {
    this.userLogged = await this.userProfile
  }


  async changeIonToggleBiometricAccess(): Promise<boolean> {
    try {
      // Get value inverted from Storage
      const boolIsEnabledBiometricAccess = !await this.isEnabledBiometricAccess();
      const ionToggleBiometricAccess = document.getElementById('ionToggleBiometricAccess');
      if (ionToggleBiometricAccess) {
        ionToggleBiometricAccess.setAttribute('checked', String(boolIsEnabledBiometricAccess));
      }
      await this.setBiometricAccess(boolIsEnabledBiometricAccess);
      return boolIsEnabledBiometricAccess;
    } catch (error) {
      console.error(error);
    }
  }

  async isEnabledBiometricAccess() {
    return await JSON.parse(this.storage.get('biometric_access'));
  }

  async checkIfFingerprintIsAvailable(): Promise<boolean> {
    try {
      if (await this.fingerprint.isAvailable() === 'biometric') {
        return true;
      }
      return false;
    } catch (error) {
      console.error('[checkIfFingerprintIsAvailable]:', error);
      return false;
    }
  }

  async showFingerprint(): Promise<any> {
    try {
      const resultShow = await this.fingerprint.show(this.fingerprintOptions);
      if (resultShow === 'biometric_success') {
        await this.goToHome();
      }
      return resultShow;
    } catch (error) {
      console.error('[showFingerprint]:', error);
    }
  }

  async checkIfBiometricAccessIsActivated() {
    if (await this.api.getAccessToken() && await this.isEnabledBiometricAccess() && await this.checkIfFingerprintIsAvailable()) {
      return true;
    }
    return false;
  }

  async login(objBody: BodyRequestLogin) {
    // Show Loading
    await this.notifications.showLoading();
    this.geolocationService.getCurrentPosition().then((resp) => {
      objBody.coordinates = {
        latitude: resp.coords.latitude,
        longitude: resp.coords.longitude
      };
    }).catch((error) => {
      this.notifications.showToast('Erro ao obter localização', 'danger')
    });
    this.api.post('/login', objBody).subscribe(async data => {

      await this.notifications.dismissLoading();
      // Set User object
      this.userLogged = data;
      // Save user_profile on Storage
      await this.saveUser(this.userLogged);
      // Publish on observable
      this.obvEventLogin.next(this.userLogged);
      // Go to home
      await this.goToHome();
      // Enable access by biometric if is available
      await this.setBiometricAccess(await this.checkIfFingerprintIsAvailable());
      this.notifications.showToast(data.message, 'success')

    }, error => {
      error = error.error;
      if (error.status == 401 || error.status == 500 || error.status == 503) {
        this.notifications.showToast(error.message, 'danger')
      }
      this.notifications.dismissLoading();
    });
  }

  async logout() {
    await this.notifications.showLoading();
    const strAccessToken = await this.api.getAccessToken();
    this.api.post(`/logout`, { token: strAccessToken }).subscribe(data => {
      this.notifications.showToast(data.message, 'success')
    }, error => {
      this.notifications.showToast(error.message, 'success')
    })
    await this.notifications.dismissLoading();
    this.storage.clearAllStorage();
    this.boolIsLoggedIn = false;
    this.menuCtrl.enable(false);
    this.navCtrl.navigateRoot('login');
  }

  async recoverPassword(objBody: { login: string }) {
    // Show Loading
    await this.notifications.showLoading();
    return this.api.post('/resetar-senha', objBody).subscribe(
      async (response) => {
        console.log(response)
        // Dismiss Loading
        await this.notifications.dismissLoading();
        if (response.status === 200) {
          this.notifications.showToast(response.message, 'success');
          this.navCtrl.navigateRoot('login');
        } else if (response.status === 500) {
          this.notifications.showToast(response.message, 'danger');
        } else if (response.status === 400) {
          this.notifications.showToast(response.message, 'danger');
        } else {
          this.notifications.showToast(this.messageError, 'danger');
        }
        return response;
      }
    );
  }

  async goToHome() {
    // Enable menu of application
    await this.menuCtrl.enable(true);
    // Redirect to home
    await this.navCtrl.navigateRoot('/home');
  }

  updatePhoto(foto: any) {
    return this.api.post('/usuario/atualizar-foto', { foto: foto }).subscribe(
      async (response) => {
        // Dismiss Loading
        await this.notifications.dismissLoading();
        localStorage.removeItem('user')
        localStorage.setItem('user', JSON.stringify(response.usuario))
        this.notifications.showToast(response.message, 'success');
        return response.usuario.path_perfil;
      }, error => {
        if (error.status == 401) {
          this.navCtrl.navigateRoot('/login');
        } else if (error.status == 500) {
          this.notifications.showToast(error.message, "danger")
        }
      }
    );
  }

  removePhoto() {
    return this.api.post('/usuario/remover-foto', {}).subscribe(
      async (response) => {
        // Dismiss Loading
        await this.notifications.dismissLoading();
        localStorage.removeItem('user')
        localStorage.setItem('user', JSON.stringify(response.usuario))
        this.notifications.showToast(response.message, 'success');
        return response.usuario.path_perfil;
      }, error => {
        if (error.status == 401) {
          this.navCtrl.navigateRoot('/login');
        } else if (error.status == 500) {
          this.notifications.showToast(error.message, "danger")
        }
      }
    );
  }

  /**
  * return Observable do usuario logado
  */
  me(): Observable<any> {
    return this.api.get('/usuario/me');
  }

  async updatePassword(user: any) {
    await this.notifications.showLoading();

    this.api.post('/usuario/atualizar-senha', user).subscribe(
      async (response) => {
        // Dismiss Loading
        await this.notifications.dismissLoading();
        if (response.status == 200) {
          this.notifications.showToast(response.message, 'success');
        }
        await this.notifications.dismissLoading();
        return response;
      }, async error => {
        if (error.error.status == 500) {
          this.notifications.showToast(error.error.body.message, 'danger');
        } else if (error.error.status == 401) {
          this.navCtrl.navigateRoot('login');
        } else if (error.error.status == 422) {
          for (const item of error.error.fields.senha) {
            this.notifications.showToast(item, "danger")
          }
        } else {
          this.notifications.showToast("Erro inesperado ao atualizar a senha, tente novamente mais tarde", 'danger');
        }
        await this.notifications.dismissLoading();
      }
    );;
  }
}
