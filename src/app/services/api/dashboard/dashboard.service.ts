import { StorageService } from './../../storage/storage.service';
import { NavController } from '@ionic/angular';
import { ApiService } from './../api.service';
import { Injectable } from '@angular/core';
import { NotificationsService } from '../../notifications/notifications.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private api: ApiService,
  ) { }


  myDashboard() {
    return this.api.get('/dashboard')
  }
}
