import { StorageService } from './../storage/storage.service';
/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { ResponseApi } from 'src/app/interfaces/response-api';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  secretToken = environment.secretToken;
  public response: ResponseApi = {
    status: 0,
    body: ''
  };

  constructor(
    private http: HttpClient,
    private storageService: StorageService,
  ) { }

  /**
   * Method HTTP POST
   * @param endpoint string
   * @param data any
   * @return Observable<any>
   */
  post(endpoint: string, data: any): Observable<any> {
    return this.http.post(this.getServer() + endpoint, data, { 'headers': this.getHttpOptions() });
  }

  /**
   * Method HTTP GET
   * @param endpoint string
   * @return Observable<any>
   */
  get(endpoint: string): Observable<any> {
    return this.http.get(this.getServer() + endpoint, { 'headers': this.getHttpOptions() });
  }


  /**
   * @function getHttpOptions
   * @description To struct object HttpHeaderOptions
   * @author Iago Rocha
   */
  getHttpOptions(): any {
    return new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      'Accept': 'application/json;',
      'X-Secret-Token': environment.secretToken,
      'Authorization': 'Bearer ' + this.storageService.get('token'),
    })
  }

  /**
   * @function handleResponse
   * @description Handle response API
   * @author Iago Rocha
   */
  handleResponse(): (response: any) => Observable<any> {
    return (response: any): Observable<any> => {
      this.response = {
        status: response.status,
        body: response.body
      };
      return of(this.response);
    };
  }

  /**
   * @function handleError
   * @description To handle error response
   * @author Iago Rocha
   */
  handleError(): (error: any) => Observable<any> {
    return (error: any): Observable<any> => {
      this.response = {
        status: error.status,
        body: error.error
      };
      // Let the app keep running by returning an empty result.
      return of(this.response);
    };
  }

  getAccessToken() {
    return this.storageService.get('token');
  }

  getServer() {
    const server = this.storageService.get('server');
    if (server == "1") {
      return environment.server_prod + '/api';
    } else if (server == "0") {
      return environment.server_hom + '/api';
    } else {
      return server ? server : environment.server_prod;
    }
  }
}
