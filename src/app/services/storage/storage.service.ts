import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(
  ) {
  }

  public set(key: string, value: any) {
    return window.localStorage.setItem(key, value);
  }

  public get(key: string) {
    return window.localStorage.getItem(key);
  }

  public remove(key: string) {
    return window.localStorage.remove(key);
  }

  public clearAllStorage() {
    const clearStorage = window.localStorage.clear();
    window.localStorage.setItem('server', '1');
    return clearStorage;
  }
}
