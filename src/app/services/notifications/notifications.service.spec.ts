import { TestBed } from '@angular/core/testing';

import { NotificationsService } from './notifications.service';
import { ToastController, LoadingController } from '@ionic/angular';

describe('NotificationsService', () => {
  const toastCtrlSpy = jasmine.createSpyObj('ToastController', ['create']);
  const toastCreateSpy = jasmine.createSpyObj('toast', ['present']);
  toastCtrlSpy.create.and.returnValue(new Promise((resolve) => { resolve(toastCreateSpy); }));
  const loadingCtrlSpy = jasmine.createSpyObj('LoadingController', ['create', 'dismiss', 'getTop']);
  const loadingCreateSpy = jasmine.createSpyObj('loading', ['present']);
  loadingCtrlSpy.create.and.returnValue(new Promise((resolve) => { resolve(loadingCreateSpy); }));
  loadingCtrlSpy.dismiss.and.returnValue(new Promise((resolve) => { resolve(true); }));
  loadingCtrlSpy.getTop.and.returnValue(new Promise((resolve) => { resolve(true); }));

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: ToastController, useValue: toastCtrlSpy },
      { provide: LoadingController, useValue: loadingCtrlSpy }
    ]
  }));

  it('should be created', () => {
    const service: NotificationsService = TestBed.inject(NotificationsService);
    expect(service).toBeTruthy();
  });

  it('method showToast', async () => {
    const service: NotificationsService = TestBed.inject(NotificationsService);
    expect(await service.showToast('Testing', 'success')).toBeTruthy();
  });

  it('method showLoading', async () => {
    const service: NotificationsService = TestBed.inject(NotificationsService);
    expect(await service.showLoading()).toBeTruthy();
  });

  it('method dismissLoading', async () => {
    const service: NotificationsService = TestBed.inject(NotificationsService);
    expect(await service.dismissLoading()).toBeTruthy();
  });

  it('method handleErrorDismiss', () => {
    const service: NotificationsService = TestBed.inject(NotificationsService);
    expect(service.handleErrorDismiss()).toBeTruthy();
  });
});
