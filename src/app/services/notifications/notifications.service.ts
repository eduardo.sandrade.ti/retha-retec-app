import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  objSetTimeout;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController
  ) {}

  /**
   * @function showToast
   * @description Control show toast
   * @author Iago Rocha
   * @param strMessage
   * @param strColor
   * @param intDuration (default = 3000)
   * @param strPosition (default = 'top') 'top' | 'middle' | 'bottom'
   * @returns
   */
  async showToast(strMessage: string, strColor: string, intDuration?: number, strPosition?: 'top' | 'middle' | 'bottom'): Promise<boolean> {
    try {
      strPosition = strPosition || 'top';
      intDuration = intDuration || 3000;
      const toast = await this.toastCtrl.create({
        message: strMessage,
        duration: intDuration,
        position: strPosition,
        color: strColor
      });

      await toast.present();
      return true;
    } catch (error) {
      return false;
    }
  }

  /**
   * @function showLoading
   * @description Control show loading
   * @author Iago Rocha
   * @param
   * @returns
   */
  async showLoading(): Promise<boolean> {
    try {
      if (!await this.loadingCtrl.getTop()) {
        // Create Object Loading
        const loading = await this.loadingCtrl.create({
          cssClass: 'loading',
          mode: 'ios'
        });
        // Present Loading
        await loading.present();
      }
      return true;
    } catch (error) {
      return false;
    }
  }

  /**
   * @function dismissLoading
   * @description Control loading dismiss
   * @author Iago Rocha
   * @param
   * @returns
   */
  async dismissLoading(): Promise<boolean> {
    try {
      await this.loadingCtrl.dismiss();
      return true;
    } catch (error) {
      return this.handleErrorDismiss();
    }
  }

  /**
   * @function handleErrorDismiss
   * @description Handle error on loading dismiss
   * @author Iago Rocha
   * @param
   * @returns
   */
  handleErrorDismiss(): boolean {
    try {
      clearTimeout(this.objSetTimeout);
      this.objSetTimeout = setTimeout(async () => {
        await this.loadingCtrl.dismiss();
      }, 3000);
      return true;
    } catch (error) {
      return false;
    }
  }
}
