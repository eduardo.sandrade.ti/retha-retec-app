import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../services/api/auth/authentication.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.page.html',
  styleUrls: ['./password-recovery.page.scss'],
})
export class PasswordRecoveryPage implements OnInit {
  formPasswordRecovery: FormGroup;
  appVersion = environment.appVersion;

  constructor(
    private auth: AuthenticationService,
    private formBuilder: FormBuilder,
    public router: Router
  ) { }

  ngOnInit() {
    this.formPasswordRecovery = this.formBuilder.group({
      login: ['', [Validators.required, Validators.minLength(8)]],
    });
  }

  async onSubmit() {
    await this.auth.recoverPassword(this.formPasswordRecovery.value);
  }

}
