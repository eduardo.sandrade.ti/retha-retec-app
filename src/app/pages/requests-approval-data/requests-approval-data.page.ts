import { StorageService } from './../../services/storage/storage.service';
import { ApiService } from './../../services/api/api.service';
import { Directory, Filesystem, FilesystemDirectory } from '@capacitor/filesystem';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';
import { DocumentViewerOptions, DocumentViewerOriginal, DocumentViewer } from '@ionic-native/document-viewer';
import { NotificationsService } from './../../services/notifications/notifications.service';
import { IonMenu, ModalController, NavController } from '@ionic/angular';
import { RequestAnexos } from './../../interfaces/request';
import { RequestService } from './../../services/api/requests/request.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Request, RequestComplemento } from 'src/app/interfaces/request';
import { environment } from 'src/environments/environment';
import { Plugins } from '@capacitor/core';
import { Storage } from '@capacitor/storage';
import { ConfirmModalComponent } from 'src/app/components/confirm-modal/confirm-modal.component';

export const FILE_KEY = 'files'
@Component({
  selector: 'app-requests-approval-data',
  templateUrl: './requests-approval-data.page.html',
  styleUrls: ['./requests-approval-data.page.scss'],
})

export class RequestsApprovalDataPage implements OnInit {
  @ViewChild('menu') menu: IonMenu;

  openMenu() {
    this.menu.open();
  }
  request: Request = { hash_code: "" };
  complemento: RequestComplemento;
  vezDeAprovar: any;
  title: string = "Aprovação";
  anexos: RequestAnexos[];
  urlServer: string = this.apiService.getServer();
  dataAprovar: any = {
    aprovar: '',
    grupo: '',
    motivo: '',
    anexo: ''
  };
  nameAnexoUpload: string = "";
  validacaoFiscal: any = [];
  modalInfoAprovacao: any = {};
  quadroAprovacao: any[] = [];
  anexoFornecedores: any[] = [];
  alertButtons = [
    {
      text: 'Cancel',
      role: 'cancel',
      handler: () => { console.log('cancel'); }
    },
    {
      text: 'OK',
      role: 'confirm',
      handler: () => { console.log('confirm'); }
    }
  ];

  downloadUrl = ''
  servicos = []
  myFiles = [];
  downloadProgress = 0;
  duplaValidacao = {};
  empresa = {};

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private requestService: RequestService,
    private navController: NavController,
    private notifications: NotificationsService,
    private modalController: ModalController,
    private http: HttpClient,
    private storageService: StorageService,
    private fileOpener: FileOpener,
    private modalCtrl: ModalController,
    private router:Router
  ) { }

  ngOnInit() {
    var urlAtual = window.location.href;
    var urlClass = new URL(urlAtual);
    if (urlAtual.includes('history')) {
      this.title = "Histórico"
    }
    this.request.hash_code = this.route.snapshot.paramMap.get('req');
    this.getRequest();

    this.loadFiles();
  }
  handleRefresh(event) {
    this.requestService.getRequest(this.request.hash_code).subscribe(data => {
      this.request = data.requisicoes;
      this.duplaValidacao = data.dupla_validacao;
      this.validacaoFiscal = data.validacao_fiscal;
      this.quadroAprovacao = data.quadro_aprovacao;
      this.anexoFornecedores = data.anexos_fornecedores;
      this.empresa = data.empresa;
      this.servicos = data.servicos;
      console.log(this.servicos)
      if (this.request.plano_de_conta.aprovacao_tecnica == "0") {
        for (let index = 0; index < this.quadroAprovacao.length; index++) {
          if (this.quadroAprovacao[index].grupo.tecnica == "1") {
            this.quadroAprovacao.splice(index, 1)
          }
        }
      }
      this.complemento = data.complemento;
      this.anexos = data.anexos;
      this.vezDeAprovar = data.vez_de_aprovar;
      event.target.complete();
    }, error => {
      if (error.status == 401) {
        this.navController.navigateRoot('/login');
      } else if (error.status == 500) {
        this.notifications.showToast(error.message, "danger")
      }
      event.target.complete();
    })
  }
  getRequest() {
    this.requestService.getRequest(this.request.hash_code).subscribe(data => {
      this.request = data.requisicoes;
      this.duplaValidacao = data.dupla_validacao;
      this.validacaoFiscal = data.validacao_fiscal;
      this.quadroAprovacao = data.quadro_aprovacao;
      this.anexoFornecedores = data.anexos_fornecedores;
      this.empresa = data.empresa;
      this.servicos = data.servicos;
      if (this.request.plano_de_conta.aprovacao_tecnica == "0") {
        for (let index = 0; index < this.quadroAprovacao.length; index++) {
          if (this.quadroAprovacao[index].grupo.tecnica == "1") {
            this.quadroAprovacao.splice(index, 1)
          }
        }
      }
      this.complemento = data.complemento;
      this.anexos = data.anexos;
      this.vezDeAprovar = data.vez_de_aprovar;
    }, error => {
      if (error.status == 401) {
        this.navController.navigateRoot('/login');
      } else if (error.status == 500) {
        this.notifications.showToast(error.message, "danger")
      }
    })
  }

  async loadFiles() {
    const videoList = await Storage.get({ key: FILE_KEY });
    this.myFiles = JSON.parse(videoList.value) || []
  }

  convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result)
    }
    reader.readAsDataURL(blob)
  })


  getMimeType(name) {
    if (name.indexOf('pdf') >= 0) {
      return 'application/pdf';
    } else if (name.indexOf('png') >= 0) {
      return 'image/png';
    } else if (name.indexOf('mp4') >= 0) {
      return 'video/mp4';
    }
  }



  aprovarOuReprovar(aprovar: boolean) {
    if (!this.dataAprovar.motivo) {
      this.notifications.showToast('Informe o motivo', 'danger')
      return false;
    }

    if (this.servicos) {
      for (const serv of this.servicos) {
        if (!serv.checked || serv.checked == false) {
          this.notifications.showToast('Para prosseguir, é necessário assinalar todos os serviços', 'danger')
          this.resetModals();
          return false;
        }
      }
    }

    this.notifications.showLoading();
    this.dataAprovar.aprovar = aprovar;
    this.dataAprovar.grupo = this.vezDeAprovar.id;
    this.requestService.aprovar(this.request.hash_code, this.dataAprovar).subscribe(data => {
      this.notifications.showToast(data.message, "success");
      this.vezDeAprovar = null;
      this.resetModals();
      this.getRequest();
      this.notifications.dismissLoading();
    this.router.navigate([`/home`]);
  }, error => {
      if (error.status == 401) {
        this.navController.navigateRoot('/login');
      } else if (error.status == 500) {
        this.notifications.showToast(error.message, "danger")
      }
      this.notifications.dismissLoading();
    })

  }

  async resetModals() {
    await this.modalController.dismiss();
  }

  inputFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      this.nameAnexoUpload = event.target.files[0].name.substring(0, 20) + '...'
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.dataAprovar.anexo = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  removeArquivo() {
    this.nameAnexoUpload = null;
    this.dataAprovar.anexo = null;
  }

  openModalInfoQuadroAprovacao(aprov: any) {
    const modal: any = document.querySelector('#open-modal-info-quadro-aprovacao');
    modal.click();
    this.modalInfoAprovacao = {};
    this.modalInfoAprovacao = aprov;
  }

  viewAnx(caminho: string) {
    this.downloadProgress = 0
    this.downloadUrl = caminho
    this.http.get(this.downloadUrl, {
      responseType: 'blob',
      reportProgress: true,
      observe: 'events',
      headers: this.requestService.getHttpOptions()
    }).subscribe(async event => {
      if (event.type === HttpEventType.DownloadProgress) {
        this.downloadProgress = Math.round((100 * event.loaded) / event.total)
      } else if (event.type === HttpEventType.Response) {
        this.downloadProgress = 0

        const name = this.downloadUrl.substring(this.downloadUrl.lastIndexOf('/') + 1)
        const base64 = await this.convertBlobToBase64(event.body) as string

        const savedFile = await Filesystem.writeFile({
          path: name,
          data: base64,
          directory: Directory.Documents
        })
        const path = savedFile.uri;
        console.log(path)
        const mimeType = this.getMimeType(name)

        this.fileOpener.open(path, mimeType)
          .then(() => { console.log('Abriu arquivo') })
          .catch(error => console.log('Erro ao abrir arquivo', error))

        this.myFiles.unshift(path)

        Storage.set({
          key: FILE_KEY,
          value: JSON.stringify(this.myFiles)
        })
      }
    })
  }

  async confirmDuplaValidacao() {
    const modal = await this.modalCtrl.create({
      component: ConfirmModalComponent,
      componentProps: {
        message: 'Tem certeza que deseja fazer isso?'
      }, cssClass: 'confirm-modal'
    });
    modal.onDidDismiss().then((result) => {
      if (result.data) {
        this.enviarDuplaValidacao()
      } else {
        // Usuário cancelou a ação
      }
    });
    await modal.present();
  }

  enviarDuplaValidacao() {
    this.notifications.showLoading();
    this.requestService.aprovar(this.request.hash_code, { dupla_aprovacao: true }).subscribe(data => {
      this.notifications.showToast(data.message, "success");
      this.vezDeAprovar = null;
      this.resetModals();
      this.getRequest();
      this.notifications.dismissLoading();
    }, error => {
      if (error.status == 401) {
        this.navController.navigateRoot('/login');
      } else if (error.status == 500) {
        this.notifications.showToast(error.message, "danger")
      }
      this.notifications.dismissLoading();
    })
  }
}
