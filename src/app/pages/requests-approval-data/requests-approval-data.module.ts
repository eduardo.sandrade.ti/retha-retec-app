import { AppSharedModules } from './../../app-shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RequestsApprovalDataPageRoutingModule } from './requests-approval-data-routing.module';

import { RequestsApprovalDataPage } from './requests-approval-data.page';

@NgModule({
  imports: [
    AppSharedModules,
    CommonModule,
    FormsModule,
    IonicModule,
    RequestsApprovalDataPageRoutingModule
  ],
  declarations: [RequestsApprovalDataPage]
})
export class RequestsApprovalDataPageModule {}
