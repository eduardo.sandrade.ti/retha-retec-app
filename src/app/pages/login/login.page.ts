import { StorageService } from './../../services/storage/storage.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { App } from '@capacitor/app';

import { AuthenticationService } from '../../services/api/auth/authentication.service';
import { environment } from './../../../environments/environment';
import { MenuController, IonRouterOutlet, Platform, NavController, CheckboxCustomEvent } from '@ionic/angular';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  formLogin: FormGroup;
  boolShowPassword = false;
  appVersion = environment.appVersion;
  showIconBiometricAccess = false;
  canDismiss = "1";
  serverDev = environment.server_hom;
  presentingElement = null;
  constructor(
    private auth: AuthenticationService,
    private formBuilder: FormBuilder,
    private menuCtrl: MenuController,
    private platform: Platform,
    private routerOutlet: IonRouterOutlet,
    private storageService: StorageService,
    protected navCtrl: NavController
  ) {
    this.platform.backButton.subscribeWithPriority(-1, () => {
      if (!this.routerOutlet.canGoBack()) {
        console.log('Login - Click Button back event... ');
        App.exitApp();
      }
    });
  }

  ngOnInit() {
    this.presentingElement = document.querySelector('.ion-page');
    this.storageService.clearAllStorage();
    this.menuCtrl.enable(false);
    this.formLogin = this.formBuilder.group({
      login: ['', [Validators.required, Validators.minLength(8)]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
    this.storageService.set('server', "1");
  }

  async ionViewDidEnter() {
    // Check if the user is logged and biometric_access is activated
    this.showIconBiometricAccess = await this.auth.checkIfBiometricAccessIsActivated();
    const lottie = (window as any).lottie;
    if (this.platform.is('hybrid') && lottie) {
      await lottie.splashscreen.on('lottieAnimationEnd', () => {
        if (this.showIconBiometricAccess) {
          this.showFingerprint();
        }
      });
    }
  }

  async onSubmit() {
    await this.auth.login(this.formLogin.value);
  }

  showFingerprint() {
    this.auth.showFingerprint();
  }

  onServerChanged(event) {
    this.storageService.set('server', event);
  }

  onInputServerChanged() {
    this.storageService.set('server', this.serverDev + "/api");
  }
}
