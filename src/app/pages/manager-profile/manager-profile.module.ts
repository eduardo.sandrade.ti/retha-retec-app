import { AppSharedModules } from 'src/app/app-shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManagerProfilePageRoutingModule } from './manager-profile-routing.module';

import { ManagerProfilePage } from './manager-profile.page';

@NgModule({
  imports: [
    AppSharedModules,
    CommonModule,
    FormsModule,
    IonicModule,
    ManagerProfilePageRoutingModule
  ],
  declarations: [ManagerProfilePage]
})
export class ManagerProfilePageModule {}
