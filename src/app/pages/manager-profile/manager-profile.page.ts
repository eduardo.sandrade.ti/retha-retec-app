import { NotificationsService } from './../../services/notifications/notifications.service';
import { Router } from '@angular/router';
import { User } from './../../interfaces/user';
import { ApplicationRef, Component, OnInit } from '@angular/core';

import { AuthenticationService } from '../../services/api/auth/authentication.service';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-manager-profile',
  templateUrl: './manager-profile.page.html',
  styleUrls: ['./manager-profile.page.scss'],
})
export class ManagerProfilePage implements OnInit {
  fingerprintIsAvailable = false;
  isEnabledBiometricAccess: boolean;
  strUserName = '-';
  photoUser = '';
  strLastSession =new Date().toLocaleString("pt-BR", { timeZone: "America/Sao_Paulo" });

  boolShowNewPassword: boolean;
  boolShowPassword: boolean;

  constructor(
    public auth: AuthenticationService,
    private navCtrl: NavController,
    private notifications: NotificationsService,
    private appRef: ApplicationRef
  ) { }

  user: any = {
    senha_atual: "",
    senha: "",
    senha_confirma: "",
  }
  async ngOnInit() {
    this.fingerprintIsAvailable = await this.auth.checkIfFingerprintIsAvailable();
    this.isEnabledBiometricAccess = await this.auth.isEnabledBiometricAccess();
    this.auth.me().subscribe(data => {
      this.strUserName = data.contato.nome
      localStorage.setItem('user', JSON.stringify(data.usuario));
      this.photoUser = data.usuario.path_perfil;
      this.appRef.tick();
    }, error => {
      if (error.status && error.status == 401) {
        this.navCtrl.navigateRoot('/login');
      }
    })
  }


  async changeIonToggleBiometricAccess() {
    this.isEnabledBiometricAccess = await this.auth.changeIonToggleBiometricAccess();
  }

  async updatePassword() {
    if (this.user.senha_confirma == this.user.senha) {
      if (this.user.senha_confirma.length >= 6) {
        await this.auth.updatePassword(this.user)
        this.user.senha_atual = "";
      } else {
        this.notifications.showToast('A senha precisa ter no mínimo 6 caracteres', 'danger');
      }
    } else {
      this.notifications.showToast('As senhas não são iguais', 'danger');
    }
  }
}
