import { Router } from '@angular/router';
import { NotificationsService } from './../../services/notifications/notifications.service';
import { StorageService } from './../../services/storage/storage.service';
import { DashboardService } from './../../services/api/dashboard/dashboard.service';
import { Component, OnInit } from '@angular/core';
import { IonRouterOutlet, Platform, NavController } from '@ionic/angular';
import { App } from '@capacitor/app';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  constructor(
    private platform: Platform,
    private routerOutlet: IonRouterOutlet,
    private router: Router,
    private dashboardService: DashboardService,
    private storageService: StorageService,
    private navController: NavController,
    private notifications: NotificationsService,
  ) {
    this.platform.backButton.subscribeWithPriority(-1, () => {
      if (!this.routerOutlet.canGoBack()) {
        console.log('Click Button back event... ')
        App.exitApp();
      }
    });
  }

  dashboard: any = []

  ngOnInit() {
    this.searchDashboardStorage();
    this.platform.ready();
    this.dashboardService.myDashboard().subscribe(async data => {
      await this.storageService.set("dashboard", JSON.stringify(data.dashboard))
      this.dashboard = data.dashboard
      this.storageService.set("permissions", JSON.stringify(data.permissions));
    }, error => {
      if (error.status == 401) {
        this.navController.navigateRoot('/login');
      } else if (error.status == 500) {
        this.notifications.showToast(error.message, "danger")
      }
    });
  }

  changeInterface(url: string) {
    this.router.navigate([url]);
  }

  searchDashboardStorage() {
    this.dashboard = JSON.parse(this.storageService.get('dashboard'));
  }

  handleRefresh(event) {
    this.searchDashboardStorage();
    this.platform.ready();
    this.dashboardService.myDashboard().subscribe(async data => {
      await this.storageService.set("dashboard", JSON.stringify(data.dashboard))
      this.dashboard = data.dashboard
      this.storageService.set("permissions", JSON.stringify(data.permissions));
      event.target.complete();
    }, error => {
      if (error.status == 401) {
        this.navController.navigateRoot('/login');
      } else if (error.status == 500) {
        this.notifications.showToast(error.message, "danger")
      }
      event.target.complete();
    });
  }
}
