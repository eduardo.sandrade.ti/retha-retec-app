import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-request',
  templateUrl: './requests.page.html',
  styleUrls: ['./requests.page.scss'],
})
export class RequestsPage implements OnInit {

  constructor(
    private navController: NavController,
    private router: Router
  ) { }

  ngOnInit() {
  }

  changeInterface(url: string) {
    this.router.navigate([url]);
    // this.navController.navigateForward(url, { replaceUrl: true });
  }

}
