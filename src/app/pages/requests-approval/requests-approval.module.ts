import { AppSharedModules } from './../../app-shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RequestsApprovalPageRoutingModule } from './requests-approval-routing.module';

import { RequestsApprovalPage } from './requests-approval.page';

@NgModule({
  imports: [
    AppSharedModules,
    CommonModule,
    FormsModule,
    IonicModule,
    RequestsApprovalPageRoutingModule
  ],
  declarations: [RequestsApprovalPage]
})
export class RequestsApprovalPageModule {}
