import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestsApprovalPage } from './requests-approval.page';

const routes: Routes = [
  {
    path: '',
    component: RequestsApprovalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestsApprovalPageRoutingModule {}
