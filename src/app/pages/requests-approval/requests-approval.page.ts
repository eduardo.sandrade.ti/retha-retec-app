import { Router } from '@angular/router';
import { Request } from './../../interfaces/request';
import { NotificationsService } from './../../services/notifications/notifications.service';
import { NavController } from '@ionic/angular';
import { RequestService } from './../../services/api/requests/request.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-requests-approval',
  templateUrl: './requests-approval.page.html',
  styleUrls: ['./requests-approval.page.scss'],
})
export class RequestsApprovalPage implements OnInit {

  interface = 0;
  requests: Request[] | any = [];
  requestNotFound: any = false;
  constructor(
    private requestService: RequestService,
    private navController: NavController,
    private notifications: NotificationsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.requestService.myRequests().subscribe(data => {
      if (data.requisicoes.length == 0) {
        this.requestNotFound = true;
      } else {
        this.requests = data.requisicoes;
        this.requestNotFound = false;
      }
    }, error => {
      if (error.status == 401) {
        this.navController.navigateRoot('/login');
      } else if (error.status == 500) {
        this.notifications.showToast(error.message, "danger")
      }
    })
  }

  changeInterface(intInterface) {
    this.interface = intInterface;
  }

  changeRequest(hashCode: string) {
    this.router.navigate([`/requests-approval/${hashCode}`]);
  }

  handleRefresh(event) {
    this.requestService.myRequests().subscribe(data => {
      if (data.requisicoes.length == 0) {
        this.requestNotFound = true;
      } else {
        this.requests = data.requisicoes;
        this.requestNotFound = false;
      }
      event.target.complete();
    }, error => {
      if (error.status == 401) {
        this.navController.navigateRoot('/login');
      } else if (error.status == 500) {
        this.notifications.showToast(error.message, "danger")
      }
      event.target.complete();
    })
  };
}
