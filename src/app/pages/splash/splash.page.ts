import { NavController } from '@ionic/angular';
import { NotificationsService } from './../../services/notifications/notifications.service';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { StorageService } from './../../services/storage/storage.service';
import { AuthenticationService } from './../../services/api/auth/authentication.service';
import { NavigationExtras, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage implements OnInit {

  constructor(
    public navCrtl: NavController,
    public storageService: StorageService,
    private fingerprintAIO: FingerprintAIO,
    private notification: NotificationsService
  ) {
    setTimeout(() => {
      this.startApp()
    }, 3000);
  }

  appVersion = environment.appVersion;
  ngOnInit() {
  }

  async startApp() {
    const navigationExtras: NavigationExtras = {
      skipLocationChange: false
    };
    if (this.storageService.get('token')) {
      if (this.fingerprintAIO.isAvailable() && this.storageService.get('biometric_access') == 'true') {
        this.showBiometria()
      } else {
        this.navCrtl.navigateRoot(['home'], navigationExtras);
      }
    } else {
      this.navCrtl.navigateRoot(['login'], navigationExtras)
    }
  }

  async showBiometria() {
    const navigationExtras: NavigationExtras = {
      skipLocationChange: false
    };
    try {
      await this.fingerprintAIO.show({
        title: 'Autenticação biométrica',
        subtitle: 'Toque no sensor de impressão digital',
        description: 'Toque no sensor para continuar',
        fallbackButtonTitle: 'Use a senha',
        disableBackup: true
      })
        .then(() => {
          // Biometria correta, continuar para a próxima página.
          this.navCrtl.navigateRoot(['home'], navigationExtras);
        })
        .catch(() => {
          // Biometria incorreta ou usuário cancelou a operação.
          this.notification.showToast('Biometria não reconhecida', 'danger')
          this.navCrtl.navigateRoot('/login');
        });
      return true;
    } catch (error) {
      return false;
    }
  }
}
