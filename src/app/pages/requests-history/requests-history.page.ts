import { Router } from '@angular/router';
import { NotificationsService } from './../../services/notifications/notifications.service';
import { NavController } from '@ionic/angular';
import { RequestService } from './../../services/api/requests/request.service';
import { Component, OnInit } from '@angular/core';
import { Request } from 'src/app/interfaces/request';

@Component({
  selector: 'app-requests-history',
  templateUrl: './requests-history.page.html',
  styleUrls: ['./requests-history.page.scss'],
})
export class RequestsHistoryPage implements OnInit {
  interface = 0;
  constructor(
    private requestService: RequestService,
    private navController: NavController,
    private notifications: NotificationsService,
    private router: Router
  ) { }

  requests: Request[] = []
  requestNotFound: boolean = false
  ngOnInit() {
    this.requestService.myHistory().subscribe(data => {
      this.requests = data.requisicoes
      if (this.requests.length == 0) {
        this.requestNotFound = true
      } else {
        this.requestNotFound = false
      }
    }, error => {
      if (error.status == 401) {
        this.navController.navigateRoot('/login');
      } else if (error.status == 500) {
        this.notifications.showToast(error.message, "danger")
      }
    })
  }

  changeInterface(intInterface) {
    this.interface = intInterface;
  }

  changeRequest(hashCode: string) {
    this.router.navigate([`/requests-approval/${hashCode}?history=true`]);
  }

  handleRefresh(event) {
    this.requestService.myHistory().subscribe(data => {
      this.requests = data.requisicoes
      if (this.requests.length == 0) {
        this.requestNotFound = true
      } else {
        this.requestNotFound = false
      }
      event.target.complete();
    }, error => {
      if (error.status == 401) {
        this.navController.navigateRoot('/login');
      } else if (error.status == 500) {
        this.notifications.showToast(error.message, "danger")
      }
      event.target.complete();
    })
  };
}
