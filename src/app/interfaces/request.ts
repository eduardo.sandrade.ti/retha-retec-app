import { Empresa } from './empresa';
export interface Request {
  req?: string,
  data_hora_registro?: string,
  hash_code?: string,
  status?: string,
  usuario?: any,
  plano_de_conta?: any,
  materiais?: string,
  servicos?: string,
  ultima_revisao?: string,
  motivo?: string,
  data_prazo?: string,
  condominio?: Empresa | any
}

export interface RequestComplemento {
  complemento_infs?: string
  req_rev_id?: string
}

export interface RequestAnexos {
  caminho?: string
  nome?: string
  extensao?: string
}