/* eslint-disable @typescript-eslint/naming-convention */
export interface User {
  access_token: string;
  token_type: string;
  expires_in: number;
  data_user: {
    nome: string;
    email: string;
  };
  permissions: {
    menu: {
      menu: string;
      nome: string;
      acesso: boolean;
    }[];
    individual: {
      permissao: {
        nome: string;
        acesso: boolean;
      };
    }[];
  };

}

export interface BodyRequestLogin {
  login?: string;
  password?: string;
  password_confirmation?: string;
  coordinates?: {
    latitude: any;
    longitude: any;
  };
}

export interface UserPhoto {
  filepath: string;
  base64Data: string;
  webviewPath: string;
}
