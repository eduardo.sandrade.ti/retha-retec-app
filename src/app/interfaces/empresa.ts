export interface Empresa {
  id?: number,
  perfil_id?: number,
  status_empresa?: string,
  state_id?: number,
  city_id?: number,
  cnpj?: string,
  razao_social?: string,
  nome_fantasia?: string,
  inscricao_estadual?: string,
  email?: string
  cep?: string
  logradouro?: string
  bairro?: string
  complemento?: string
  numero?: string
  tipo?: string,
  aceite_termo?: string
  path_logo?: string
}