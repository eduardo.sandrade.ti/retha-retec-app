// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  secretToken: '61b004d2-3c1d-4dbb-a465-7f75061b8d6e',
  server_prod: "https://retec.retha.com.br", // https://retec.retha.com.br
  server_hom: "https://retecdev.retha.com.br", // http://retha-retec
  appVersion: require('../../package.json').version,
  keyNameJWT: 'access_token'
};
