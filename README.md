# DEPENDÊNCIAS DO PROJETO

```
Ionic:

   Ionic CLI                     : 6.20.1
   Ionic Framework               : @ionic/angular 6.2.7
   @angular-devkit/build-angular : 14.2.3
   @angular-devkit/schematics    : 14.2.3
   @angular/cli                  : 14.2.3
   @ionic/angular-toolkit        : 6.1.0

Capacitor:

   Capacitor CLI      : 3.8.0
   @capacitor/android : 4.0.0
   @capacitor/core    : 3.6.0
   @capacitor/ios     : 3.6.0

Utility:

   cordova-res : 0.15.4
   native-run  : 1.7.0

System:

   NodeJS : v16.16.0
   npm    : 8.11.0

```

# COMANDOS ÚTEIS

## Rodando a aplicação na web
`$ ionic serve`

## Build da aplicação Ionic

Ambiente de desenvolvimento (environment.ts):
`$ ionic build`

Ambiente de homologação (environment.sandbox.ts):
`$ ionic build --configuration=sandbox`

Ambiente de desenvolvimento (environment.prod.ts):
`$ ionic build --configuration=production`

## Build Ionic + Capacitor

`$ ionic cap build android`
`$ ionic cap build ios`


## Debug Android com Livereload

`$ ionic cap run android --livereload --external`

# EXTRAS

## Splash Screen Animada (Lottie)

> Doc. Ionic: https://ionicframework.com/docs/native/lottie-splash-screen
> Doc. Github: https://github.com/timbru31/cordova-plugin-lottie-splashscreen
> Tutorial exemplo: https://www.youtube.com/watch?v=JfX7pBq1YG4

# Possiveis Erros

## LottieSplashScreen

#### Unresolved reference: uppercase
- Vá para o arquivo localizado em:
    "android\capacitor-cordova-android-plugins\src\main\kotlin\de\dustplanet\cordova\lottie\LottieSplashScreen.kt"

- Encontre o codigo:
      preferences.getString(
                "LottieScaleType",
                "FIT_CENTER"
      ).uppercase(Locale.ENGLISH)
      
- Remova o ".uppercase(Locale.ENGLISH)" e tente novamente


#### Atualizar Capacitor
- https://capacitorjs.com/docs/updating/4-0

#### Criar uma chave de upload e atualizar keystores
- Rode esse comando:
  keytool -export -rfc -keystore "C:\wamp64\www\retha-retec-app\keystore.jks" -alias Retec -file "C:\Users\guilh\Downloads\upload_cert.pem"